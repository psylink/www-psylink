---
title: "Android: First Step"
slug: android-firststep
date: 2024-08-01
author: Roman
---

The new PsyLink GUI now builds on Android!

One part is still missing: Bluetooth connections. So, currently the app is effectively useless, as it will scan for PsyLink devices but never find any. A good first step though.

You can find the Android build instructions in the [BUILDING.md](https://codeberg.org/psylink/psylink/src/branch/master/BUILDING.md). It's quite the wrestling match.

[![Android Screenshot of PsyLink scanning for bluetooth devices, never finding any.](/img/blog/2024-08-02-android.jpg)](/img/blog/2024-08-02-android.jpg)
